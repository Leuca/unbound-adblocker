/**
 * Copyright (c) 2022 Luca Magrone
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <curl/curl.h>
#include <stdlib.h>
#include <string.h>

#include "download.h"
#include "../unbound-adblocker.h"

char adlist_hosts_url[MAX_URL] = DEFAULT_ADLIST_URL;

static size_t ua_write_memory_callback(void *data, size_t size, size_t nmemb, void *raw_download)
{
    size_t true_size = size * nmemb;
    struct raw_download *raw = (struct raw_download *)raw_download;
    
    char *tmp  = realloc(raw->mem, raw->size + true_size + 1); // grow allocated memory
    if(tmp == NULL) {
        fprintf(stderr, "Could not reallocate raw->mem\n");
        return 0;
    }
    raw->mem = tmp;
    memcpy(&(raw->mem[raw->size]), data, true_size); // append new data to existing one
    raw->size += true_size; // grow size
    raw->mem[raw->size] = 0; // null character

    return true_size;
}

int ua_download_adlist(struct raw_download *raw_download)
{
    curl_global_init(CURL_GLOBAL_ALL);

    CURL *curl = curl_easy_init();
    if(curl != NULL){
        CURLcode res;

        curl_easy_setopt(curl, CURLOPT_URL, adlist_hosts_url);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, ua_write_memory_callback);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)raw_download);

        /* some servers do not like requests that are made without a user-agent
        field, so we provide one */
        curl_easy_setopt(curl, CURLOPT_USERAGENT, "libcurl-agent/1.0");
        res = curl_easy_perform(curl);

        if(res != CURLE_OK) {
            fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
            return -1;
        }


        curl_easy_cleanup(curl);
    }
    curl_global_cleanup();
    return 0;
}

unsigned int ua_file_to_raw_download(struct raw_download *raw_download, char filename[])
{
    FILE *fp;

    fp = fopen(filename, "r");
    if (fp == NULL)
    {
        fprintf(stderr, "Unable to open file %s\n", filename);
        exit(EXIT_FAILURE);
    }

    /* Count how many characters are in the file */
    size_t count = 0;
    while (!feof(fp))
    {
        fgetc(fp);
        count++;
    }

    /* Allocate enough memory */
    raw_download->mem = realloc(raw_download->mem, count);
    raw_download->size = count;

    /* Go back to the beginnig of the file */
    rewind(fp);

    /* Do the copying */
    char c;
    count = 0;
    while (!feof(fp))
    {
        c = fgetc(fp);
        memcpy(&(raw_download->mem[count]), &c, 1);
        count++;
    }

    raw_download->mem[count] = 0;

    return count;
}
