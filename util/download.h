/**
 * Copyright (c) 2022 Luca Magrone
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef DOWNLOAD_H
#define DOWNLOAD_H

#include <string.h>

/**
 * struct raw_download - a structure to a variable size character array
 */
struct raw_download {
        char *mem; ///< A pointer to char which makes use of dynamic allocation of memory
        size_t size; ///< The size of the pointed data
};

/**
 * int ua_download_adlist(struct raw_download *raw_download) - a function that downloads a host file
 *
 * @param raw_download A struct raw_download meant to save the downloaded data
 * @return -1 on failure, 0 on success
 *
 * This function uses libcurl routines to download a host file containing a list of undesired domains which will be blocked. The resulting downloaded data is saved in a struct raw_download data type.
 */
int ua_download_adlist(struct raw_download *raw_download);

unsigned int ua_file_to_raw_download(struct raw_download *raw_download, char filename[]);

#endif
