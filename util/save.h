/**
 * Copyright (c) 2022 Luca Magrone
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef SAVE_H
#define SAVE_H

#include <stdio.h>

#include "download.h"

/**
 * unsigned int ua_hosts_to_unbound_file(struct raw_download raw_download, char filename[]) - parse hosts to an unbound drop-in configuration file
 *
 * @param raw_download A struct that contains a pointer to the hosts downloaded data
 * @param filename The name of the file to be created
 * @return the number of parsed domain names
 *
 * This funtion reads data from a raw_download.mem and extracts domain names which will be saved inside a 'filename' file with the format 'local-zone: "domain" always_nxdomain'.
 */
unsigned int ua_hosts_to_unbound_file(struct raw_download raw_download, char filename[]);

#endif
