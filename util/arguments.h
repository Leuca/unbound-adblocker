/**
 * Copyright (c) 2022 Luca Magrone
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef ARGUMENTS_H
#define ARGUMENTS_H

/**
 * void ua_read_arguments_and_parameters(int argc, char *argv[]) - function that handles command line arguments
 *
 * @param argc The number of arguments the main() function received
 * @param argv An array containing one string for each command line argument passed to the main() funciton
 *
 * This function takes care of the command line arguments and stops the program if something goes wrong.
 * It also stops the program after the setup mode is invoked. This is because the program is meant to either setup the configurations or download the ads domains list. 
 */
void ua_read_arguments_and_parameters(int argc, char *argv[]);

#endif
