/**
 * Copyright (c) 2022 Luca Magrone
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>


#include "unbound-config.h"
#include "../unbound-adblocker.h"

char conf_dir[MAX_FILENAME] = DEFAULT_CONFROOT;
char conf_file[MAX_FILENAME] = DEFAULT_CONFFILE;
char local_dir[MAX_FILENAME] = DEFAULT_LOCALDIR;
char local_conf[MAX_FILENAME] = DEFAULT_LOCALCONF;
char redirect_string[MAX_REDIRECT] = DEFAULT_REDIRECT;

int redirect = 0;

int ua_check_config_subdir() {
    struct stat ucd;

    if(stat(local_dir, &ucd) == 0)
        if(S_ISDIR(ucd.st_mode))
            return 0;

    return -1;
}

long int ua_check_unbound_configuration() {
    FILE *fp;
    char t;
    char tmp[MAX_FILENAME];
    char *ptr_tmp;

    long int i_pos = -1;
    long int disabled = 0;

    fp = fopen(conf_file, "r");
    if(fp == NULL) {
        fprintf(stderr, "Could not open %s\n", conf_file);
        return -1;
    }

    while(1) {
        t = fgetc(fp);
        if(feof(fp))
            break;

        if(t == 'i'){
            i_pos = ftell(fp);
            fseek(fp, i_pos - 1, SEEK_SET);
            if(fgets(tmp, MAX_FILENAME, fp) == NULL)
                break;
            
            if(strncmp(tmp, "include:", 8) == 0) {
                // We found an include 
                ptr_tmp = &tmp[8];
                while(*ptr_tmp == ' ' || *ptr_tmp == '\t') {
                    ptr_tmp = &ptr_tmp[1];
                }

                if(strncmp(ptr_tmp, "/etc/unbound/local.d/*.conf", 27) == 0)
                    break;
            }

            i_pos = -1;
        }
    }

    if(i_pos > -1) {
        // Check if the line we found is enabled
        fseek(fp, i_pos - 2, SEEK_SET);
        t = fgetc(fp);

        while(t == ' ' || t == '\t') {
            fseek(fp, -2, SEEK_CUR);
            t = fgetc(fp);
        }

        if(t == '#')
            disabled = 1;
    }

    fclose(fp);

    if(i_pos > -1) {
        if(!disabled)
            return disabled;
    }

    return i_pos;
}

int ua_enable_config_subdir(long int include_pos, int enable)
{
    FILE *modified, *orig;
    char renamed[MAX_FILENAME];
    int ret;

    long int orig_pos;
    char modified_c;
    char orig_c;

    char tmp_line[100];
    int already_modified = 0;

    int status = -1;

    strcpy(renamed, conf_file);
    strcat(renamed, ".orig");

    ret = rename(conf_file, renamed);
    if(ret != 0) {
        fprintf(stderr, "Error: Could not rename configuration file\n");
        return status;
    }

    modified = fopen(conf_file, "w+");
    if(modified == NULL) {
        fprintf(stderr, "Could not open %s\n", conf_file);
        return status;
    }

    orig = fopen(renamed, "r");
    if(orig == NULL) {
        fprintf(stderr, "Could not open %s\n", renamed);
        return status;
    }

    if(enable) {
        while(1) {
            orig_c = fgetc(orig);
            orig_pos = ftell(orig);
            fputc(orig_c, modified);
            if(feof(orig))
                break;

            if(orig_pos == include_pos) {
                do {
                    fseek(modified, -2, SEEK_CUR);
                    modified_c = fgetc(modified);
                } while(modified_c != '#');

                fseek(modified, -1, SEEK_CUR);
                fputc('i', modified);
            }
        }
        status = 0;
    }
    else {
        while(fgets(tmp_line, 100, orig) != NULL) {
            if(!already_modified && strcmp(tmp_line, "server:\n") == 0) {
                fprintf(modified, "server:\n\tinclude: %s/%s\n", local_dir, "*.conf");
                already_modified += 1;
                status = 0;
            }
            else {
                fputs(tmp_line, modified);
            }
        }
    }


    return status;
}

int ua_setup()
{
    long int ret;
    int enabled = 0;

    errno = 0;

    if(ua_check_config_subdir()) {
        // Create local.d subdir because it was not found
        ret = mkdir(local_dir, 0755);

        if(ret == -1) {
            switch(errno) {
                case EACCES :
                    fprintf(stderr, "You do not have permission to create the directory, try running as root\n");
                    return -1;
                        case EEXIST:
                    fprintf(stderr, "%s already exist!\n", local_dir);
                    return -1;
                        case ENAMETOOLONG:
                    fprintf(stderr, "Please choose a shorter pathname\n");
                        default:
                    fprintf(stderr, "error: mkdir\n");
                    return -1;
            }
        }

    }

        ret = ua_check_unbound_configuration();
        if(ret > 0) {
        // Uncomment parameter
                enabled = ua_enable_config_subdir(ret, 1);
        }

        if(ret < 0) {
        // Add parameter
                enabled = ua_enable_config_subdir(ret, 0);
        }

    return enabled;
}
