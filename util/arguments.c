/**
 * Copyright (c) 2022 Luca Magrone
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <curl/curl.h>

#include "../config.h"

#include "unbound-config.h"
#include "arguments.h"
#include "../unbound-adblocker.h"

extern char conf_file[MAX_FILENAME];
extern char local_conf[MAX_FILENAME];
extern char adlist_hosts_url[MAX_URL];
extern char redirect_string[MAX_REDIRECT];
extern int redirect;

char hosts_filename[MAX_FILENAME];
int hosts_is_local = 0;


char flags[4] = {'h', 's', 'V', 0};

void ua_set_unbound_config(char filename[])
{
    strcpy(conf_file, filename);
}   

void ua_set_unbound_adblocker_config(char url[])
{
    strcpy(local_conf, url);
}

void ua_set_adlist_hosts_url(char filename[])
{
    strcpy(adlist_hosts_url, filename);
}

void ua_set_hosts_file(char filename[])
{
    hosts_is_local = 1;
    strcpy(hosts_filename, filename);
}

void ua_set_redirect_string(char string[])
{
    redirect = 1;
    strcpy(redirect_string, string);
}

int ua_is_argument_flag(char argument)
{
    int is_flag = 0;
    int i = 0;

    while(flags[i] != 0) {
        if(argument == flags[i]) {
            is_flag += 1;
            break;
        }
        i += 1;
    }

    return is_flag;
}

char ua_process_argument_str(char argument[])
{
    if(argument[0] != '-')
        return 0;

    if(argument[2] != 0)
        return 0;

    return argument[1];
}

void ua_print_version()
{
    curl_version_info_data *curl_data = curl_version_info(CURLVERSION_NOW);
    fprintf(stderr, "Version %s\n\n"
            "Configure line: %s\n"
            "Linked libs: libcurl %s, %s\n"
            "\n"
            "BSD licensed, see LICENSE in source package for details.\n"
            "Report bugs to %s\n",
            VERSION,
            CONFCMDLINE,
            curl_data->version,
            curl_data->ssl_version,
            PACKAGE_BUGREPORT);
}

void ua_print_help(char program_name[])
{
    fprintf(stderr,
            "Usage:\t\t%s [options]\n"
            "\t\tdonwload a list of domains (in the format of hosts)\n\t\tand convert it in a configuration format for unbound\n\n"
            "-h\t\tprint this help message\n"
            "-c file\t\tunbound config file, default %s\n"
            "-u url\t\thost list url, default StevenBlack's \"hosts + fakenews\"\n"
            "-l file\t\tdrop-in config file, default %s\n"
            "-s\t\tsetup unbound configuration file (does not download)\n"
            "-f file\t\thosts file on disk\n"
            "-r string\tredirect string (ex: 'CNAME example.com')\n"
            "-V\t\tprint version number and build options\n\n"
            "Version %s\n"
            "BSD licensed, see LICENSE in source package for details.\n"
            "Report bugs to %s\n",
            program_name,
            DEFAULT_CONFFILE,
            DEFAULT_LOCALCONF,
            VERSION,
            PACKAGE_BUGREPORT);
}

void ua_read_arguments_and_parameters(int argc, char *argv[])
{
    int i;
    char arg_tmp;
    int is_flag;
    
    for(i = 1; i < argc; i++) {
        arg_tmp = ua_process_argument_str(argv[i]);
        if(arg_tmp == 0) {
            fprintf(stderr, "Unknown error while reading command line arguments\n");
            exit(EXIT_FAILURE);
        }

        is_flag = ua_is_argument_flag(arg_tmp);
        if(is_flag) {
            switch(arg_tmp) {
                case 'h':
                    ua_print_help(argv[0]);
                    exit(EXIT_SUCCESS);
                case 's':
                    if(ua_setup() == 0)
                        exit(EXIT_SUCCESS);
                    else
                        exit(EXIT_FAILURE);
                case 'V':
                    ua_print_version();
                    exit(EXIT_SUCCESS);
                default:
                    fprintf(stderr, "error: argument not recognised: %s\n", argv[i]);
                    ua_print_help(argv[0]);
                    exit(EXIT_FAILURE);
            }
        }
        else {
            if(i+1 < argc) {
                switch(arg_tmp) {
                    case 'c':
                        ua_set_unbound_config(argv[i+1]);
                        break;
                    case 'u':
                        ua_set_adlist_hosts_url(argv[i+1]);
                        break;
                    case 'l':
                        ua_set_unbound_adblocker_config(argv[i+1]);
                        break;
                    case 'f':
                        ua_set_hosts_file(argv[i+1]);
                        break;
                    case 'r':
                        ua_set_redirect_string(argv[i+1]);
                        break;
                    default:
                        fprintf(stderr, "error: argument not recognised: %s\n", argv[i]);
                        ua_print_help(argv[0]);
                        exit(EXIT_FAILURE);
                }

                i += 1;
            }
            else {
                fprintf(stderr, "error: argument '%s' requires a parameter\n", argv[i]);
                ua_print_help(argv[0]);
                exit(EXIT_FAILURE);
            }
        }
    }
}
