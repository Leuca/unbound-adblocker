/**
 * Copyright (c) 2022 Luca Magrone
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef UNBOUND_CONFIG_H
#define UNBOUND_CONFIG_H

/**
 * long int ua_check_unbound_configuration() - check whether local.d subdir is present
 * @return 0: found, -1: not found, a value > 0 when disabled
 *
 * A function that checks the unbound configuration file for a configuration parameter that includes the 'local.d' subdirectory.
 *
 * The function returns 0 when the configuration parameter was found, -1 when the configuration parameter was not found and the position where it was found if the parameter is disabled.
 */
long int ua_check_unbound_configuration();

/**
 * int ua_enable_config_subdir(long int include_pos, int enable) - modify unbound configuration to load config file from subdirectory
 * @param include_pos The position where 'include' was found (ignored if enable is 0)
 * @param enable Whether the configuration parameter has to be enabled or not
 * @return 0 on success, -1 on failure
 *
 * This function serves the purpose of modifying the unbound configuration file to include drop-in configurations in a subdirectory of the configuration directory (the default subdirectory is '/etc/unbound/local.d').
 *
 * It either uncomments the parameter or adds it after the 'server' directive, depending on whether enable is set to 1 or 0.
 * Remember to pass the correct position of the file where the 'i' (as in 'include') was found.
 *
 * You should call 'ua_check_unbound_configuration()' beforehand to make sure of how to call this function.
 *
 * Note: it does not overwrite the original file but rather it renames it appending the suffix '.orig'
 */
int ua_enable_config_subdir(long int include_pos, int enable);

/**
 * int ua_setup() - setup unbound configuration file
 *
 * @return -1 on failure, 0 on success
 *
 * This function ensures that the local.d directory is included in the unbound configuration.
 * The local.d directroy is a subdirectory of the unbound configuration directory where the ad-blocking configuration file will be placed.
 *
 * It calls ua_enable_config_subdir() in different ways depending on the returned value of ua_check_unbound_configuration().
 */
int ua_setup();

#endif
