/**
 * Copyright (c) 2022 Luca Magrone
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "util/download.h"
#include "util/save.h"
#include "util/arguments.h"
#include "util/unbound-config.h"
#include "unbound-adblocker.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <errno.h>

extern char local_conf[MAX_FILENAME];
extern char hosts_filename[MAX_FILENAME];
extern int hosts_is_local;

int main( int argc, char *argv[] ){
    clock_t start, end;

    struct raw_download downloaded_data;
    char **dom;
    unsigned int domains;

    int save_res;

    /* Take care of command line arguments */
    ua_read_arguments_and_parameters(argc, argv);

    /* Initialize and allocate memory */
    downloaded_data.mem = malloc(1);
    downloaded_data.size = 0;

    /* If reading from disk */
    if (hosts_is_local)
    {
        ua_file_to_raw_download(&downloaded_data, hosts_filename);
    }
    else
    {
        start = clock();
        ua_download_adlist(&downloaded_data);
        end = clock();
        printf("Downloaded hosts in %lf seconds\n", (double)(end - start)/CLOCKS_PER_SEC);
    }

    /* Save data to disk */
    printf("Saving data to disk...\n");
    start = clock();

    domains = ua_hosts_to_unbound_file(downloaded_data, local_conf);
    free(downloaded_data.mem);
    end = clock();

    printf("Generated unbound drop-in configuration with %u domains in %lf seconds\n", domains, (double)(end - start)/CLOCKS_PER_SEC);

    return 0;
}
