/**
 * Copyright (c) 2022 Luca Magrone
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef UNBOUND_ADBLOCKER_H
#define UNBOUND_ADBLOCKER_H

#define MAX_FILENAME 100
#define MAX_URL 200
#define MAX_REDIRECT 150

#define DEFAULT_ADLIST_URL "https://raw.githubusercontent.com/StevenBlack/hosts/master/alternates/fakenews/hosts"

#define DEFAULT_CONFROOT "/etc/unbound"
#define DEFAULT_LOCALDIR "/etc/unbound/local.d"
#define DEFAULT_CONFFILE "/etc/unbound/unbound.conf"
#define DEFAULT_LOCALCONF "/etc/unbound/local.d/unbound-adblocker.conf"
#define DEFAULT_REDIRECT "A 127.0.0.1"

#endif
