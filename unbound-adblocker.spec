Name:           {{{ git_dir_name }}}
Version:        {{{ git_dir_version lead=0.3 follow=1 }}}
Release:        1%{?dist}
Summary:        Simple program that makes Unbound an adblocker

License:        GPLv2+
URL:            https://gitlab.com/Leuca/unbound-adblocker
VCS:            {{{ git_dir_vcs }}}

Source:         {{{ git_dir_pack }}}

BuildRequires:  automake
BuildRequires:  autoconf
BuildRequires:  make
BuildRequires:  gcc
BuildRequires:  curl-devel
BuildRequires:  systemd-rpm-macros

Requires:   curl
Requires:   unbound

%description
This program is able to generate a drop-in configuration file for Unbound to block unwanted domain names

%prep
{{{ git_dir_setup_macro }}}

%build
autoreconf
%configure --enable-systemd
%make_build

%install
%make_install

# Quick fix for systemd units installation
%{__mkdir} -p %{buildroot}%{_unitdir}
%{__install} -m 0644 systemd/%{name}.service systemd/%{name}.timer %{buildroot}%{_unitdir}

%files
%license LICENSE
%{_bindir}/%{name}
%{_unitdir}/%{name}.service
%{_unitdir}/%{name}.timer

%changelog
{{{ git_dir_changelog }}}
